function makeUser(nameValue,ageValue,addValue,telValue,sexValue,emailValue){
	return {name: nameValue,
		age: ageValue,
		add: addValue,
		tel: telValue,
		sex: sexValue,
		email: emailValue,
		};
}

function showUser(userValue){
	let str = "";
		for (let key in userValue) {
			str = str+userValue[key]+"\n";
		}
		return str;
}

function cloneUser(userValue){
	let tempUser = {};
	for (let key in userValue) {
		tempUser[key] = userValue[key];
	}
	return tempUser;
}