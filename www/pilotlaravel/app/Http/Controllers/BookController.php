<?php

namespace App\Http\Controllers;

use App\Books;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /*function store(Request $request) {
        $book = new Books();
        $book->setAttribute("name",$request->name);
        $book->setAttribute("category",$request->category);
        $book->setAttribute("description",$request->description);
        if($book->save()) {
            return true;
        }
    }*/

    function store(Request $request) {
        $book = new Books();
        if($book::created($request->all())) {
            return true;
        }
    }

    function update(Request $request, Books $books) {
        if($books->fill($request->all())->save()) {
            return true;
        }
    }

    function index() {
        $books = Books::all();
        return $books;
    }

    function show(Book $book) {
        return $book;
    }

    function destroy(Books $books) {
        if($books->delete()) {
            return true;
        }
    }
}
