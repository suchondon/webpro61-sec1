<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('book');
});
/*
Route::get('/test', function () {
    return 'this is a testing for a new  route' ;
});

Route::get('/test1', function () {
    return 'this is a testing for a new  route' ;
});
*/